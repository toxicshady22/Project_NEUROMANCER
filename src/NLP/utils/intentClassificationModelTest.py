import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import sys 

sys.path.append(dir_path+"/..")
from intentClassification import intentClassification

while 1:
    try:
        intentClassification(input("<-What would you like to say to me?: "), "debug")
    except KeyboardInterrupt:
        print("Understood. Stopping now.......")
        sys.exit()