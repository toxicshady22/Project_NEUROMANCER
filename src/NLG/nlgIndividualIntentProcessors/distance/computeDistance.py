import spacy
import geocoder

nlp=spacy.load("en_core_web_md")

def main(inputString):
    locations=[]
    doc=nlp(inputString)
    for entity in doc.ents:
        if entity.label_=="GPE":
            locations.append(entity)
    
    #try: 
    distance=int(geocoder.distance(locations))
    #except:
    #    distance=4651
    #    print("Uh oh something went wrong, fix this.")
               
        
    return str(distance)



main("What is the distance between Ahmedabad and Chicago?")